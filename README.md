# Shopping cart demo

This repository contains some backends that are used in conjuction with the frontend, found in the same Gitlab group.  
The whole stack implements some basic shopping cart functionality.

It contains these microservices:
* shopping-basket
    * Responsible for communication with the frontend.
    * Contains the FSM logic, that dictates what transitions a cart can take.
* validation
    * Responsible for running business logic related validations
* add-product
    * Responsible for adding a product to the cart

>For faster prototyping, all backends are in the same repo, and the correct files for each microservice are loaded through profiles.  
>Since it's a PoC, the code is also poorly commented.

All communication happens through Websockets (STOMP).

Technologies used
* Spring Boot
* Spring Cloud Function
* Websockets
* Couchbase
* RabbitMQ
* Grafana
