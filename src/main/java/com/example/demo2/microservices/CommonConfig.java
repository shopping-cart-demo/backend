package com.example.demo2.microservices;


import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionRequest;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.util.MimeType;

import java.lang.reflect.Type;

@Configuration
public class CommonConfig {
    public MessageConverter customMessageConverter(ObjectMapper objectMapper) {
        return new MyCustomMessageConverter(objectMapper);
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT, JsonTypeInfo.As.PROPERTY);


        return objectMapper;
    }

    public class MyCustomMessageConverter extends AbstractMessageConverter {
        private ObjectMapper objectMapper;
        private Type type;

        public MyCustomMessageConverter(ObjectMapper objectMapper) {
            super(
                    new MimeType("application", "request"),
                    new MimeType("application", "response")
            );
            this.objectMapper = objectMapper;
        }

        @Override
        protected boolean supports(Class<?> clazz) {
            return (TransitionRequest.class.equals(clazz)) || TransitionResponse.class.equals(clazz);
        }

        @Override
        protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
            Object payload = message.getPayload();

            try {
                return this.objectMapper.readValue((byte[]) payload, targetClass);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected Object convertToInternal(Object payload, @Nullable MessageHeaders headers, @Nullable Object conversionHint) {
            try {
                return this.objectMapper.writeValueAsBytes(payload);
            } catch (Exception e) {
                return null;
            }
        }


    }
}
