package com.example.demo2.microservices;

import com.example.demo2.microservices.shoppingbasket.fsm.listeners.Utils;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionRequest;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionResponse;
import com.example.demo2.models.Cart;
import com.example.demo2.models.ValidationResult;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.function.Function;

@Slf4j
@Configuration
@Profile("validation")
public class ValidationMicroservice {

    @Bean
    public Function<TransitionRequest<Cart>, TransitionResponse<JsonNode>> validation(Utils utils) {

        return request -> {
            log.info("Received message to validate");
            try {
                Thread.sleep(2_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Cart originalCart = request.getPayload();
            Cart cart = (Cart) utils.clone(originalCart);


            ValidationResult validationResult = cart.getCustomer() == null
                    ? new ValidationResult(false, "No customer found")
                    : new ValidationResult(true, null);

            cart.setValidationResult(validationResult);

            log.info("Finished validation");

            return new TransitionResponse<>(request.getFsmId(), utils.calculateDiff(originalCart, cart));
        };
    }
}
