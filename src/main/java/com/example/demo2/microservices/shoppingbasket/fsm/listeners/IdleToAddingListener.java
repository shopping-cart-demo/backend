package com.example.demo2.microservices.shoppingbasket.fsm.listeners;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMState;
import com.example.demo2.microservices.shoppingbasket.fsm.FiniteStateMachine;
import com.example.demo2.microservices.shoppingbasket.fsm.FsmStateChangedListener;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionRequest;
import com.example.demo2.microservices.shoppingbasket.fsm.models.requests.AddProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("shopping-basket")
public class IdleToAddingListener implements FsmStateChangedListener<FiniteStateMachine, AddProductRequest> {
    @Autowired
    private Utils utils;

    @Override
    public FSMState getFrom() {
        return FSMState.IDLE;
    }

    @Override
    public FSMState getTo() {
        return FSMState.ADDING;
    }

    @Override
    public Class<AddProductRequest> getPayloadType() {
        return AddProductRequest.class;
    }

    @Override
    public void triggerTransition(FiniteStateMachine fsm, AddProductRequest payload) {
        utils.sendToExchange("add-product-microservice", new TransitionRequest<>(fsm.getId(), payload));
    }
}
