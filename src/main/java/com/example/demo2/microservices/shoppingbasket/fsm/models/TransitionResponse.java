package com.example.demo2.microservices.shoppingbasket.fsm.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransitionResponse<T> {
    private String uuid = UUID.randomUUID().toString();
    private String fsmId;
    private boolean success = true;
    private T payload;

    public TransitionResponse(String fsmId, T payload) {
        this.fsmId = fsmId;
        this.payload = payload;
    }
}
