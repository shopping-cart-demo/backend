package com.example.demo2.microservices.shoppingbasket.fsm;

public interface FsmStateChangedListener<FSM extends FiniteStateMachine, P> {

    FSMState getFrom();

    FSMState getTo();

    Class<P> getPayloadType();

    void triggerTransition(FSM fsm, P payload);
}
