package com.example.demo2.microservices.shoppingbasket.fsm.listeners;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMState;
import com.example.demo2.microservices.shoppingbasket.fsm.FiniteStateMachine;
import com.example.demo2.microservices.shoppingbasket.fsm.FsmStateChangedListener;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionRequest;
import com.example.demo2.models.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("shopping-basket")
public class AddToValidateListener implements FsmStateChangedListener<FiniteStateMachine, Cart> {
    @Autowired
    private Utils utils;

    @Override
    public FSMState getFrom() {
        return FSMState.ADDING;
    }

    @Override
    public FSMState getTo() {
        return FSMState.VALIDATING;
    }

    @Override
    public Class<Cart> getPayloadType() {
        return Cart.class;
    }

    @Override
    public void triggerTransition(FiniteStateMachine fsm, Cart payload) {
        utils.sendToExchange("validation-microservice", new TransitionRequest<>(fsm.getId(), payload));
    }
}
