package com.example.demo2.microservices.shoppingbasket;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMService;
import com.example.demo2.microservices.shoppingbasket.fsm.FiniteStateMachine;
import com.example.demo2.microservices.shoppingbasket.fsm.models.requests.AddProductRequest;
import com.example.demo2.models.AddProduct;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;

import java.util.HashMap;
import java.util.Map;

@Controller
@Slf4j
@Profile("shopping-basket")
public class WebSocketController {
    @Autowired
    private FSMService fsmService;
    @Autowired
    private SimpMessageSendingOperations messageTemplate;

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public void greeting(@Payload @Validated AddProduct payload) {
        FiniteStateMachine fsm = fsmService.createFsm();
        fsmService.triggerTransition(fsm.getId(), new AddProductRequest(fsm.getCart(), payload));
    }

    @MessageExceptionHandler(MethodArgumentNotValidException.class)
    public void handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        messageTemplate.convertAndSend("/topic/errors", "Errors:" + errors);
    }

    @MessageExceptionHandler(MessageConversionException.class)
    public void handleJsonExceptions(MessageConversionException ex) {
        Map<String, String> errors = new HashMap<>();

        if (ex.getCause() instanceof UnrecognizedPropertyException) {
            UnrecognizedPropertyException un = (UnrecognizedPropertyException) ex.getCause();
            errors.put("Unrecognised field", un.getPropertyName());
        }


        messageTemplate.convertAndSend("/topic/errors", "Errors:" + errors);
    }
}
