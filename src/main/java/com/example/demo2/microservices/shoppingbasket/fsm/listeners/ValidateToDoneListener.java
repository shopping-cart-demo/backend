package com.example.demo2.microservices.shoppingbasket.fsm.listeners;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMState;
import com.example.demo2.microservices.shoppingbasket.fsm.FiniteStateMachine;
import com.example.demo2.microservices.shoppingbasket.fsm.FsmStateChangedListener;
import com.example.demo2.models.Cart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
@Profile("shopping-basket")
public class ValidateToDoneListener implements FsmStateChangedListener<FiniteStateMachine, Cart> {
    @Autowired
    private SimpMessageSendingOperations messageTemplate;


    @Override
    public FSMState getFrom() {
        return FSMState.VALIDATING;
    }

    @Override
    public FSMState getTo() {
        return FSMState.DONE;
    }

    @Override
    public Class<Cart> getPayloadType() {
        return Cart.class;
    }

    @Override
    public void triggerTransition(FiniteStateMachine fsm, Cart payload) {
        log.info("Returning answer to client");

        if (payload.getValidationResult().isSuccess()) {
            messageTemplate.convertAndSend("/topic/greetings", fsm.getId(), new HashMap<>());
        } else {
            messageTemplate.convertAndSend("/topic/errors", "Validation has failed.", new HashMap<>());
        }

    }

}
