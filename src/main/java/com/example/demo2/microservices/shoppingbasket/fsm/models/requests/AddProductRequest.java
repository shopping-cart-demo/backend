package com.example.demo2.microservices.shoppingbasket.fsm.models.requests;

import com.example.demo2.models.AddProduct;
import com.example.demo2.models.Cart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddProductRequest {
    private Cart cart;
    private AddProduct addProduct;
}
