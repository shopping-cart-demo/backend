package com.example.demo2.microservices.shoppingbasket;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMService;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.function.Consumer;

@Profile("shopping-basket")
@Configuration
@Slf4j
public class ShoppingBasketMicroservice {

    @Bean
    public Consumer<TransitionResponse<?>> transition(FSMService fsmService) {
        return transitionResponse -> {
            fsmService.triggerTransition(transitionResponse.getFsmId(), transitionResponse.getPayload());
        };
    }

}
