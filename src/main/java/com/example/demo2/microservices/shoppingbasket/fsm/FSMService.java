package com.example.demo2.microservices.shoppingbasket.fsm;

import com.example.demo2.db.FsmRepository;
import com.example.demo2.microservices.shoppingbasket.fsm.listeners.Utils;
import com.example.demo2.microservices.shoppingbasket.fsm.models.Transition;
import com.example.demo2.models.AddProduct;
import com.example.demo2.models.Cart;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Profile("shopping-basket")
public class FSMService {
    @Autowired
    private FsmRepository fsmRepository;
    @Autowired
    private List<FsmStateChangedListener> fsmStateChangedListeners;
    @Autowired
    private Utils utils;
    @Autowired
    private SimpMessageSendingOperations messageTemplate;

    private Map<String, FiniteStateMachine> fsmMap = new HashMap<>();

    public FiniteStateMachine createFsm() {
        FiniteStateMachine fsm = new FiniteStateMachine();
        fsm.getTransitions().addAll(Arrays.asList(
                new Transition<>(FSMState.IDLE, FSMState.ADDING, AddProduct.class)
        ));

        return fsmRepository.save(fsm);
    }

    public <T extends FiniteStateMachine> void triggerTransition(String fsmId, Object payload) {
        FiniteStateMachine fsm = fsmRepository.findById(fsmId).get();
        if (fsm.getCurrentState() == FSMState.DONE) {
            return;
        }

        Object newPayload = completePreviousTransition(fsm, payload);

        final FiniteStateMachine updatedFsm = fsmRepository.save(fsm);

        fsmStateChangedListeners
                .stream()
                .filter(x -> x.getFrom() == updatedFsm.getCurrentState())
                .findFirst()
                .ifPresent(x -> {
                    x.triggerTransition(updatedFsm, newPayload);
                    updatedFsm.setCurrentState(x.getTo());
                });

        fsmRepository.save(updatedFsm);
    }

    public <T extends FiniteStateMachine> Object completePreviousTransition(FiniteStateMachine fsm, Object payload) {
        Cart originalCart = fsm.getCart();
        boolean changed = false;
        if (payload instanceof JsonNode) {
            messageTemplate.convertAndSend("/topic/diffs", payload);

            Cart cart1 = utils.applyDiff(originalCart, (JsonNode) payload);
            fsm.setCart(cart1);
            changed = true;
        }

        fsm.getTimeline().add(utils.writeValueAsString(fsm.getCart()));

        return changed
                ? fsm.getCart()
                : payload;
    }

}
