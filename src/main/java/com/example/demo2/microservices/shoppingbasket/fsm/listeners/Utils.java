package com.example.demo2.microservices.shoppingbasket.fsm.listeners;

import com.example.demo2.models.Cart;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Utils {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    public void sendToExchange(String exchange, Object payload) {
        try {
            rabbitTemplate.convertAndSend(exchange, "", objectMapper.writeValueAsString(payload), new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    //manually set the content type, as it's text/plain by default
                    message.getMessageProperties().setContentType("application/request");
                    return message;
                }
            });
        } catch (Exception e) {

        }
    }

    public JsonNode calculateDiff(Cart oldCart, Cart newCart) {
        try {
            JsonNode jsonNodeOld = this.objectMapper.readValue(this.objectMapper.writeValueAsString(oldCart), JsonNode.class);
            JsonNode jsonNodeNew = this.objectMapper.readValue(this.objectMapper.writeValueAsString(newCart), JsonNode.class);
//            return com.github.fge.jsonpatch.diff.JsonDiff.asJson(jsonNodeOld, jsonNodeNew);
            return JsonDiff.asJson(jsonNodeOld, jsonNodeNew);
        } catch (Exception e) {
            return null;
        }

    }

    public Cart applyDiff(Cart cart, JsonNode diffs) {
        try {
            JsonNode json = this.objectMapper.readValue(this.objectMapper.writeValueAsString(cart), JsonNode.class);
//            JsonNode apply = com.github.fge.jsonpatch.JsonPatch.fromJson(diffs).apply(json);
            JsonNode apply = JsonPatch.apply(diffs, json);
            return this.objectMapper.readValue(this.objectMapper.writeValueAsString(apply), Cart.class);
        } catch (Exception e) {
            return null;
        }
    }

    public Object clone(Object toClone) {
        try {
            return this.objectMapper.readValue(this.objectMapper.writeValueAsString(toClone), toClone.getClass());
        } catch (Exception e) {
            return null;
        }
    }

    public String writeValueAsString(Object object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new PotatoException();
        }
    }

    public class PotatoException extends RuntimeException {

    }
}
