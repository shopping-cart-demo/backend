package com.example.demo2.microservices.shoppingbasket.fsm.models.requests;

import com.example.demo2.models.Cart;
import lombok.Data;

@Data
public class BaseRequest {
    private Cart cart;
}
