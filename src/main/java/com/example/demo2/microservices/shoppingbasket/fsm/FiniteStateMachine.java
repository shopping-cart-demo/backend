package com.example.demo2.microservices.shoppingbasket.fsm;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.example.demo2.microservices.shoppingbasket.fsm.models.Transition;
import com.example.demo2.models.Cart;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Document
@Data
@NoArgsConstructor
public class FiniteStateMachine implements Serializable {
    @Id
    public String id = "fsm:" + UUID.randomUUID().toString();
    @Field
    private FSMState currentState = FSMState.IDLE;
    @Field
    private Cart cart = new Cart();
    @Field
    private List<Transition<?>> transitions = new ArrayList<>();
    @Field
    private List<String> timeline = new ArrayList<>(Collections.singletonList("{}")); //start with empty json
    @Version
    private Long version;
}
