package com.example.demo2.microservices.shoppingbasket.fsm.models;

import com.example.demo2.microservices.shoppingbasket.fsm.FSMState;
import lombok.Data;

@Data
public class Transition<T> {
    public FSMState from;
    public FSMState to;
    public Class<T> payloadType;

    public Transition(FSMState from, FSMState to, Class<T> payloadType) {
        this.from = from;
        this.to = to;
        this.payloadType = payloadType;
    }
}
