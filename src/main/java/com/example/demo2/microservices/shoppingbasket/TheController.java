package com.example.demo2.microservices.shoppingbasket;

import com.example.demo2.db.FsmRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fsm")
@Slf4j
public class TheController {
    @Autowired
    private FsmRepository fsmRepository;

    @GetMapping(path = "{fsmId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<List<String>> createCart(@PathVariable("fsmId") String fsmId){
        return ResponseEntity.ok(fsmRepository.findById(fsmId).get().getTimeline());
    }
}
