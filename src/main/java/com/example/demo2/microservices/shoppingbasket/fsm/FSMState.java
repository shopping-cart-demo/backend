package com.example.demo2.microservices.shoppingbasket.fsm;

public enum FSMState {
    IDLE,
    ADDING,
    VALIDATING,
    DONE,
    ERROR
}
