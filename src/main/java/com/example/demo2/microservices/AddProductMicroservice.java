package com.example.demo2.microservices;

import com.example.demo2.microservices.shoppingbasket.fsm.listeners.Utils;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionRequest;
import com.example.demo2.microservices.shoppingbasket.fsm.models.TransitionResponse;
import com.example.demo2.microservices.shoppingbasket.fsm.models.requests.AddProductRequest;
import com.example.demo2.models.Cart;
import com.example.demo2.models.Customer;
import com.example.demo2.models.Product;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.function.Function;


@Slf4j
@Profile("add-product")
@Configuration
public class AddProductMicroservice {

    @Bean
    public Function<TransitionRequest<AddProductRequest>, TransitionResponse<JsonNode>> addProduct(Utils utils) {

        return request -> {
            try {
                Thread.sleep(2_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Cart originalCart = request.getPayload().getCart();
            Cart cart = (Cart) utils.clone(originalCart);
            cart.getProducts().add(new Product(request.getPayload().getAddProduct().getProductId()));
            if (request.getPayload().getAddProduct().isValidCustomer()) {
                cart.setCustomer(new Customer("John", "Doe"));
            }

            log.info("Finished adding cart");
            return new TransitionResponse<>(request.getFsmId(), utils.calculateDiff(originalCart, cart));
        };
    }

}
