package com.example.demo2.db;

import com.example.demo2.microservices.shoppingbasket.fsm.FiniteStateMachine;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.N1qlSecondaryIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

@Repository
@N1qlPrimaryIndexed
@N1qlSecondaryIndexed(indexName = "the_secondary_index")
public interface FsmRepository extends CouchbaseRepository<FiniteStateMachine, String> {

}
