package com.example.demo2.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class Customer implements Serializable {
    private String id = UUID.randomUUID().toString();
    public String name;
    public String surname;

    public Customer(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
