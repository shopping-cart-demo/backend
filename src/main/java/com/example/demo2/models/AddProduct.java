package com.example.demo2.models;

import lombok.Data;
import org.springframework.data.couchbase.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document
@Data
public class AddProduct implements Serializable {
    @NotNull
    private String productId;
    private String sessionId;
    @NotNull
    private boolean validCustomer;
    @Min(1)
    private Long amount;
}
