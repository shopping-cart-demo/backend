package com.example.demo2.models;


import com.couchbase.client.java.repository.annotation.Field;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class Cart implements Cloneable, Serializable {
    @Field
    private Customer customer;
    @Field
    private List<Product> products = new ArrayList<>();
    @Field
    private ValidationResult validationResult;

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
